const tabNav = document.querySelector('.tabs');
const allTextContent = document.querySelectorAll('.tab');
tabNav.addEventListener('click', function (event) {
    const tabMenu = document.querySelector('.active-tab');
    tabMenu.classList.remove('active-tab');
    allTextContent.forEach(item => {
        item.classList[item.dataset.paragraph === event.target.dataset.navname ? 'add' : 'remove']('active-paragraph');
    });
    event.target.classList.add('active-tab');
});




// function tab() {
//     let tabNav = document.querySelectorAll('.tabs-title');
//     let tabContent = document.querySelectorAll('.tab');
//
//     tabContent.forEach(element =>{
//         element.classList.add('hidden');
//     });
//     let tabDataName;
//     tabNav.forEach(item => {
//         item.addEventListener('click', selectTabNav);
//     });
//     function selectTabNav(e) {
//         tabNav.forEach(item => {
//             item.classList.remove('active');
//         });
//         console.log(e.target);
//         e.target.classList.add('active');
//         tabDataName = e.target.getAttribute('data-navname');
//         function getTabContent(tabDataName) { // функция, которая добавляет 'active' в случае совпадения data-tab-name
//             tabContent.forEach(element => {
//                 if(element.classList.contains(tabDataName)) {
//                     element.classList.add('active');
//                 } else {
//                     element.classList.remove('active');
//                 }
//             });
//         }
//         getTabContent(tabDataName);
//     }
// }
// tab();
